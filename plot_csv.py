import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

sns.set_context("talk")

def color_mapping(x):

    if "latexrun" in x: 
        return "#FEE440"
    if "latexmk" in x: 
        return "#00F5D4"
    if ("rubber" in x) or ("scons" in x) or ("arara" in x):
        return "#F15BB5"
    if "dvi" in x:
        return "#9B5DE5"
        
    return "#2E4057"
    #"latexmk": "#00F5D4"


def assign_scores(sorted_data, column):
    # Create a dictionary to store the scores
    scores = {}
    eurovision_scores = [12, 10, 8, 7, 6, 5, 4, 3, 2, 1, 0]

    # Initialize a score counter
    score_index = 0

    # Iterate through the sorted DataFrame
    prev_score = None
    for _, row in sorted_data.iterrows():

        name = row['name']
        value = row[column]

        if score_index > len(eurovision_scores)-1:
            scores[name] = 0
        else:   
            scores[name] = eurovision_scores[score_index]

        if prev_score and (value != prev_score):    
            score_index += 1
        
        prev_score = value

    return scores

# Read the CSV file
csv_file = "benchmark_results.csv"
data = pd.read_csv(csv_file)

# Calculate the mean time for each subdirectory
mean_times = data.groupby("name")["time"].mean().reset_index()
sorted_data = data.merge(mean_times, on="name", suffixes=("", "_mean"))

# Sort the data based on the mean time
sorted_data = sorted_data.sort_values(by="time_mean")

palette = {k: color_mapping(k) for k in sorted_data.name.unique() }
scores = assign_scores(mean_times.sort_values(by="time"), "time")
print(scores)
result = {k: scores[k] for k in scores}

print("Time")
print(scores)
# Plot the bar graph with error bars
fig, ax = plt.subplots(1,1, figsize=(16, 10))

ax = sns.barplot(y="name", x="time", data=sorted_data, palette=palette, errwidth=1, capsize=.05, errorbar=('ci', 95))

ax.set_ylabel(None)
ax.set_xlabel("Average time [s]")

plt.tight_layout()

sns.despine(ax=ax)
# Show the plot
#plt.show()
plt.savefig("build_times.png")
plt.savefig("build_times.svg")

# Plot the bar graph with error bars
fig, ax = plt.subplots(1,1, figsize=(16, 10))

# Calculate the mean time for each subdirectory
mean_stdout_count = data.groupby("name")["stdout_count"].mean().reset_index()
sorted_data = data.merge(mean_stdout_count, on="name", suffixes=("", "_mean"))

# Sort the data based on the mean time
sorted_data = sorted_data.sort_values(by="stdout_count_mean")
scores = assign_scores(mean_stdout_count.sort_values(by="stdout_count"), "stdout_count")
print("stdout_count_mean")
print(scores)
result = {k: result[k] + scores.get(k, 0) for k in scores}

ax = sns.barplot(y="name", x="stdout_count", data=sorted_data, palette=palette, errwidth=1, capsize=.05, errorbar=None)

ax.set_ylabel(None)
ax.set_xlabel("Average number of lines to stdout")

plt.tight_layout()

sns.despine(ax=ax)
# Show the plot
#plt.show()
plt.savefig("build_stdout.png")
plt.savefig("build_stdout.svg")

# Plot the bar graph with error bars
fig, ax = plt.subplots(1,1, figsize=(16, 10))

# Calculate the mean time for each subdirectory
mean_filesize = data.groupby("name")["filesize"].mean().reset_index()
sorted_data = data.merge(mean_filesize, on="name", suffixes=("", "_mean"))

# Sort the data based on the mean time
sorted_data = sorted_data.sort_values(by="filesize_mean")
scores = assign_scores(mean_filesize.sort_values(by="filesize"), "filesize")
print("filesize")
print(scores)

result = {k: result[k] + scores.get(k, 0) for k in scores}

sorted_data["filesize"] /= 1024
ax = sns.barplot(y="name", x="filesize", data=sorted_data, palette=palette, errwidth=1, capsize=.05, errorbar=None)

ax.set_ylabel(None)
ax.set_xlabel("File size [kB]")

plt.tight_layout()

sns.despine(ax=ax)
# Show the plot
#plt.show()
plt.savefig("build_filesize.png")
plt.savefig("build_filesize.svg")


# Plot the bar graph with error bars
fig, ax = plt.subplots(1,1, figsize=(16, 10))

# Calculate the mean time for each subdirectory
maketarget_count = data.groupby("name")["maketarget"].mean().reset_index()
sorted_data = data.merge(maketarget_count, on="name", suffixes=("", "_mean"))

# Sort the data based on the mean time
sorted_data = sorted_data.sort_values(by="maketarget_mean")
scores = assign_scores(maketarget_count.sort_values(by="maketarget"), "maketarget")
print("maketarget")
print(scores)

result = {k: result[k] + scores.get(k, 0) for k in scores}

ax = sns.barplot(y="name", x="maketarget", data=sorted_data, palette=palette, errwidth=1, capsize=.05, errorbar=None)

#ax.set_xticks(range(10))
ax.set_ylabel(None)
ax.set_xlabel("Number of characters in Make target")

plt.tight_layout()

sns.despine(ax=ax)
# Show the plot
#plt.show()
plt.savefig("build_make.png")
plt.savefig("build_make.svg")

# Plot the bar graph with error bars
fig, ax = plt.subplots(1,1, figsize=(16, 10))

result_sorted = dict(sorted(result.items(), key=lambda x:x[1]))
print(result_sorted)

keys = list(result_sorted.keys())
vals = [value for value in result_sorted.values()]

ax = sns.barplot(y=keys, x=vals, palette=palette)
plt.gca().invert_yaxis()

#ax = sns.barplot(y="name", x="maketarget", data=sorted_data, palette=palette, errwidth=1, capsize=.05, errorbar=None)

ax.set_ylabel(None)
ax.set_xlabel("Total score")

plt.tight_layout()

sns.despine(ax=ax)
# Show the plot
#plt.show()
plt.savefig("build_scores.png")
plt.savefig("build_scores.svg")
#plt.show()