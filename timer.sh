#!/bin/bash

# Number of times to run the command
num_runs=20

# Output CSV file
csv_file="benchmark_results.csv"

# Remove the CSV file if it already exists
if [ -f "$csv_file" ]; then
    rm "$csv_file"
fi

# Write CSV header
echo "name,time,stdout_count,filesize,maketarget" >> "$csv_file"

# Iterate through subdirectories
for dir in */; do
    dir=${dir%/}  # Remove trailing slash
    
    echo "Setting up files for benchmark $dir..."
    # Make sure additions are added to the top of each file.
    if [ "$dir" == "preamble" ] || [ "$dir" == "latexrun_preamble" ]; then

        echo "Preamble detected for $dir"
        cat preamble.tex > $dir/preamble.tex
        echo "\\endofdump" >> $dir/preamble.tex
        echo "%&preamble" > $dir/test.tex
        echo "\\endofdump" >> $dir/test.tex
        cat test.tex >> $dir/test.tex
        cat refs.bib > $dir/refs.bib
    else
        cat $dir.tex preamble.tex test.tex > $dir/test.tex
    fi

    cp refs.bib $dir/
done

# Run the command multiple times
for ((i=1; i<=$num_runs; i++)); do
    for dir in */; do
    #for dir in latexrun latexrun_ramdisk; do
        dir=${dir%/}  # Remove trailing slash

        echo "Running benchmark iteration $i for $dir ..."

        /usr/bin/time -o timing make -C "$dir" clean test.pdf | tee martin.log

        real_time=$(cat timing | grep real | awk '{print $1}' | sed 's/,/\./')
        stdout_count=$(cat martin.log | wc -l)

        filesize=$(stat -f%z $dir/test.pdf)
        maketarget=$(perl count_lines.pl $dir)

        # Write data to CSV
        echo "$dir,$real_time,$stdout_count,$filesize,$maketarget" >> "$csv_file"
    done
    
done
