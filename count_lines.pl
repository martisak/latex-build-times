#!/usr/bin/perl

# Get the target name from the command-line argument
my $dir = shift;
my $target_name = "test.pdf";

# Read the Makefile content
my $makefile_content;
{
    local $/;
    open(my $fh, '<', "$dir/Makefile") or die "Cannot open Makefile: $!";
    $makefile_content = <$fh>;
    close($fh);
}

# Count the lines in the target section of the Makefile
my $line_count = -1;
my $target_found = 0;

foreach my $line (split /\n/, $makefile_content) {

    # In target
    if ($line =~ /^\s*$target_name\s*:/) {
        $target_found = 1;
    # In target and not finding whitespace
    } elsif ($line =~ /^[^\s]/ && $target_found) {
        $target_found = 0;
        last;
    } elsif ($target_found) {
        $line_count += length($line);
    }
}

print "$line_count";
