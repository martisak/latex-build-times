SUBDIRS := $(wildcard */.)


all: build_times.png
benchmark_results.csv:
	./timer.sh

build_times.png: plot_csv.py benchmark_results.csv
	pipenv run python3 $<

clean:
	-rm benchmark_results.csv build_times.png

	for dir in $(SUBDIRS); do \
        $(MAKE) -C $$dir clean; \
    done

distclean:
	for dir in $(SUBDIRS); do \
        rm -f $$dir/test.tex $$dir/refs.bib $$dir/martin.log; \
    done